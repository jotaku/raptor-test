﻿using System.Collections;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class RaptorAnimation : MonoBehaviour
{
    public bool playOnAwake = false;
    public float delay = 0f;
    public Animator[] animators;
    public bool resetAnimation = false;

    public float animationTime;
    public GameObject finalDino;
    public bool resetedAnimation = false;
    public Vector3 finalPosition;
    public TouchRaptorController touchRaptorController;

    private void Start()
    {
        if(playOnAwake)
            StartCoroutine(PlayAnimationRoutine());
    }

    private void Update()
    {
        if (resetAnimation)
            ResetRaptorAnimation();

        touchRaptorController.transform.localPosition = finalDino.transform.localPosition;
    }

    IEnumerator PlayAnimationRoutine()
    {
        yield return new WaitForSeconds(delay);

        foreach (Animator a in animators)
        {
            a.SetBool("Animate", true);
        }
        yield return new WaitForSeconds(animationTime);
        if (!resetedAnimation)
        {
            finalDino.GetComponent<Animator>().SetBool("Final", true);
            yield return new WaitForEndOfFrame();
            //finalDino.transform.localPosition =  finalPosition;
            
            touchRaptorController.gameObject.SetActive(true);
        }
        else
        {
            resetedAnimation = false;
        }
    }

    public void PlayRaptorAnimation()
    {
        StartCoroutine(PlayAnimationRoutine());
    }

    public void ResetRaptorAnimation()
    {
        foreach (Animator a in animators)
        {
            a.SetBool("Animate", false);
        }
        resetedAnimation = true;
        //finalDino.transform.localPosition = Vector3.zero; 
    }
}
