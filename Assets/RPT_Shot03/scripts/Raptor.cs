﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class Raptor : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip[] roars;
    public AudioClip[] screechs;
    public AudioClip[] growls;
    public AudioClip[] calls;
    public string[] randomAnimations = { "Bite","Growl" };
    private Animator anim;

    private void OnEnable()
    {
        anim = GetComponent<Animator>();
    }

    IEnumerator PlayRoar()
    {
        yield return new WaitForSeconds(1);

        if (roars != null)
        { 
            audioSource.clip = roars[Random.Range(0, roars.Length)];
            audioSource.Play();
        }
    }

    IEnumerator PlayScreech()
    {
        yield return new WaitForSeconds(1);

        if (screechs != null)
        { 
            audioSource.clip = screechs[Random.Range(0, screechs.Length)];
            audioSource.Play();
        }
    }

     IEnumerator PlayGrowls()
    {
        yield return new WaitForSeconds(0.6f);
        if (growls != null)
        { 
            audioSource.clip = growls[Random.Range(0, growls.Length)];
            audioSource.Play();
        }
    }

    IEnumerator PlayCalls()
    {
        yield return new WaitForSeconds(1);

        if (calls != null)
        {
            audioSource.clip = calls[Random.Range(0, calls.Length)];
            audioSource.Play();
        }
    }

    public void PlayRandomAnimation()
    {
        string random = randomAnimations[Random.Range(0, randomAnimations.Length)];
        anim.SetTrigger(random);

        switch (random)
        {
            case "Bite":
                StartCoroutine(PlayScreech());
            break;
            case "Growl":
                StartCoroutine(PlayGrowls());
            break;
        }
        
    }

}
