﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchRaptorController : MonoBehaviour
{
    private void OnMouseDown()
    {
        PlayAnimationRaptor();
    }

    private RaptorAnimation GetRaptor()
    {
        RaptorAnimation[] raptors = FindObjectsOfType<RaptorAnimation>();
        for (int i = 0; i < raptors.Length; i++)
        {
            if (raptors[i].gameObject.activeInHierarchy)
                return raptors[i];
        }

        return null;
    }

    private void PlayAnimationRaptor()
    {
        RaptorAnimation r = GetRaptor();
        if (r != null)
        {
            Debug.Log(r.gameObject.name);
            r.finalDino.GetComponent<Raptor>().PlayRandomAnimation();
        }
    }
}
