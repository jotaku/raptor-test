﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PhotoController : MonoBehaviour
{
    public GameObject[] uiToDisable;
    public AudioSource cameraSound;

    public void OnClickScreenCaptureButton()
    {
        StartCoroutine(CaptureScreen());
    }
    public IEnumerator CaptureScreen()
    {
        yield return null;

        EnableDisableUI(false);

        yield return new WaitForEndOfFrame();

        ScreenCapture.CaptureScreenshot("raptor_photo.png");
        cameraSound.Play();

        yield return new WaitForSeconds(3f);

        EnableDisableUI(true);
    }
    public void SharePhoto()
    {
        string filePath = Path.Combine(Application.persistentDataPath, "raptor_photo.png");

        new NativeShare().AddFile(filePath)
        .SetCallback((result, shareTarget) => Debug.Log("Share result: " + result + ", selected app: " + shareTarget))
        .Share();
    }

    private void EnableDisableUI(bool enable)
    {
        foreach (GameObject g in uiToDisable)
        {
            g.SetActive(enable);
        }
    }

}
