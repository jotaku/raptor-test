﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoCheckpoint : MonoBehaviour
{
    private AudioSource audio;
    private Dino dinoPlayer;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        dinoPlayer = FindObjectOfType<Dino>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && !audio.isPlaying)
        {
            audio.Play();
            dinoPlayer.IncreaseCorrectJumps();
        }
    }
}
