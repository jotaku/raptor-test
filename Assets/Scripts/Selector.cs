﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Selector : MonoBehaviour
{

    public GameObject[] items;
    public GameObject[] placeholders;
    public string[] scenes;


    GameObject currentItem;
    int currentIndex;
    // Start is called before the first frame update
    void Start()
    {
        currentItem = items[0];
        currentIndex = 0;
        items[0].SetActive(true);
        for (int i = 1; i < items.Length; i++)
        {
            items[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Next()
    {
        items[currentIndex].SetActive(false);

        currentIndex++;
        if(currentIndex >= items.Length)
        {
            currentIndex = 0;
        }
        items[currentIndex].SetActive(true);
    }

    public void Previous()
    {
        items[currentIndex].SetActive(false);

        currentIndex--;
        if (currentIndex < 0)
        {
            currentIndex = items.Length - 1;
        }
        items[currentIndex].SetActive(true);
    }

    public void SetPlaceholders()
    {
        for (int i = 0; i < placeholders.Length; i++)
        {
            placeholders[i].SetActive(false);
        }
        placeholders[currentIndex].SetActive(true);
        SceneManager.LoadScene(scenes[currentIndex]);

    }
}
