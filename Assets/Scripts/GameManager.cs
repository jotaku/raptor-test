﻿using Doozy.Engine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public string url = "https://raptorserver-s7jlmydnpa-uc.a.run.app/";

    public Ruleta ruleta;

    public GameObject loading;

    public GameObject formView;

    public GameObject rouleteView;

    public string idRegion;

    public bool detectCountry;
    // Start is called before the first frame update
    void Start()
    {
        if (detectCountry)
        {
            StartCoroutine(DetectCountryAsync());
        }

        // Uncomment this method to enable OneSignal Debugging log output 
        // OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.INFO, OneSignal.LOG_LEVEL.INFO);

        // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
        OneSignal.StartInit("2fb31262-98f4-4494-9668-ba14c1861ad4")
          .HandleNotificationOpened(HandleNotificationOpened)
          .Settings(new Dictionary<string, bool>() {
      { OneSignal.kOSSettingsAutoPrompt, false },
      { OneSignal.kOSSettingsInAppLaunchURL, false } })
          .EndInit();

        OneSignal.inFocusDisplayType = OneSignal.OSInFocusDisplayOption.Notification;

        // The promptForPushNotifications function code will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission.
        OneSignal.PromptForPushNotificationsWithUserResponse(OneSignal_promptForPushNotificationsResponse);


    }

    private void OneSignal_promptForPushNotificationsResponse(bool accepted)
    {
        Debug.Log("OneSignal_promptForPushNotificationsResponse: " + accepted);
    }


    // Gets called when the player opens the notification.
    private static void HandleNotificationOpened(OSNotificationOpenedResult result)
    {
    }


    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator DetectCountryAsync()
    {
        var httpClient = new HttpClient();
        UnityWebRequest www = UnityWebRequest.Get("https://api.ipdata.co/country_code?api-key=a6d8b6393d7efa543e322ca0bc5aec52cb4dcf62e9b7538df6833e1d");
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {

            Debug.Log($"My country is: {www.downloadHandler.text}");
            SetCountry(www.downloadHandler.text);


        }
    }

    public void SetCountry(string cc)
    {
        idRegion = cc;
        StartCoroutine(GetPrizes());
    }

    IEnumerator GetPrizes()
    {
        WWWForm form = new WWWForm();
        form.AddField("idregion", idRegion);
        UnityWebRequest www = UnityWebRequest.Post(url + "users/getpryzes", form);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            StartCoroutine(GetPrizes());
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            PrizesList act = JsonUtility.FromJson<PrizesList>(www.downloadHandler.text);
            ruleta.SetPrizes(act.data);
        }
    }

    internal void Participate(string name, string email, string dpi, string phone)
    {
        loading.SetActive(true);
        StartCoroutine(ParticipateRoutine(name, email, dpi, phone));
    }

    IEnumerator ParticipateRoutine(string name, string email, string dni, string phone)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", name);
        form.AddField("email", email);
        form.AddField("dni", dni);
        form.AddField("phone", phone);
        form.AddField("idregion", idRegion);

        UnityWebRequest www = UnityWebRequest.Post(url + "users/", form);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            StartCoroutine(ParticipateRoutine(name, email, dni, phone));
        }
        else
        {
            Debug.Log(www.downloadHandler.text);

            if (www.responseCode == 500)
            {
                Debug.Log("ERROR EN EL REGISTRO");
            }
            else
            {
                loading.SetActive(false);
                formView.GetComponent<UIView>().Hide();
                rouleteView.GetComponent<UIView>().Show();
                Response res = JsonUtility.FromJson<Response>(www.downloadHandler.text);
                ruleta.Participate(res.iduser, res.token);

            }
        }
    }

    internal void SpinRoulete(int iduser, string token)
    {
        StartCoroutine(SpinRouleteRoutine(iduser, token));
    }

    IEnumerator SpinRouleteRoutine(int iduser, string token)
    {
        WWWForm form = new WWWForm();
        form.AddField("iduser", iduser);

        UnityWebRequest www = UnityWebRequest.Post(url + "users/pryze", form);
        www.SetRequestHeader("Authorization", "Bearer " + token);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            StartCoroutine(SpinRouleteRoutine(iduser, token));

        }
        else
        {
            Debug.Log(www.downloadHandler.text);

            if (www.responseCode == 500)
            {
                Debug.Log("ERROR EN EL GET PRICE");
            }
            else
            {
                ResponsePrizeData res = JsonUtility.FromJson<ResponsePrizeData>(www.downloadHandler.text);
                ruleta.Response(res.data);

            }
        }
    }

    public void ContinueFromRoulete()
    {
        SceneManager.LoadScene("Main");

    }


}
