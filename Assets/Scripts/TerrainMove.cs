﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class TerrainMove : MonoBehaviour
{
    public GameObject objects;
    public GameObject target;
    public float distanceOfPlayer = 10f;
    public float speed = 1f;

    public Transform restartPosition;

    private DinoGameManager gameManager;

    private void Awake()
    {
        gameManager = FindObjectOfType<DinoGameManager>();
    }

    private void Update()
    {
        Debug.Log("Is Behind ? " + IsBehindPlayer());

        if (gameManager.GameStart && !IsBehindPlayer())
        {
            objects.transform.Translate(Vector3.forward * Time.deltaTime * speed, target.transform);
        }
        else if (IsBehindPlayer())
        {
            objects.transform.localPosition = restartPosition.position;
        }

    }

    private bool IsBehindPlayer()
    {
        Vector3 toTarget = (objects.transform.position - target.transform.position + (Vector3.forward * distanceOfPlayer)).normalized;

        if (Vector3.Dot(toTarget, target.transform.forward) > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
