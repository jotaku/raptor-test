﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class RaptorTrackableEventHandler : DefaultTrackableEventHandler
{
    public GameObject placeButton;
    public GameObject placeholder;
    public GameObject model;

    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();

        //    raptorTrackerHandler.raptorAnimation.gameObject.SetActive(true);
        //arController.currentAnimation = raptorTrackerHandler.raptorAnimation;
        //arManager.GoToPreCamera();
        //playButton.interactable = true;
        //selector.SetActive(false);

        placeButton.SetActive(true);
        placeholder.SetActive(false);
        model.SetActive(true);
        
    }

    protected override void OnTrackingLost()
    {
      //  Debug.Log("OnTrackingLost");

      //  foreach (TouchRaptorController trc in raptrosControllers)
      //  { 
      //      trc.gameObject.SetActive(false);
      //  }
      //  arController.ResetAnimations();
      ////  raptorTrackerHandler.raptorAnimation.gameObject.SetActive(false);
      //  playButton.interactable = false;
      //  selector.SetActive(true);

        base.OnTrackingLost();
    }
}
