﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ARVelociraptor : MonoBehaviour
{
    public string animation;

    private Animator anim;


    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.touchCount >= 1)
        {
            Vector2 vTouchPos = Input.GetTouch(0).position;

            Ray ray = Camera.main.ScreenPointToRay(vTouchPos);

            RaycastHit hit;
            if (Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                if (hit.transform.tag == "Raptor")
                {
                    PlayAnimation();
                }
            }
        }
    }

    public void PlayAnimation()
    {
        anim.SetBool(animation,true);
    }

    public void StopAnimation()
    {
        anim.SetBool(animation, false);
    }

}
