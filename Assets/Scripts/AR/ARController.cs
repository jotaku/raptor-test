﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class ARController : MonoBehaviour
{
    public RaptorAnimation[] raptorAnimations;
    public RaptorAnimation currentAnimation;

    public Button playButton;

    public GameObject placeHolderLata;

    public Button locatorButton;


    public GameObject[] onScanObjects;

    public GameObject[] onCameraObjects;

    private void Update()
    {
        //GetCurrentAnimation();
    }

    private void GetCurrentAnimation()
    {
        foreach (RaptorAnimation ra in raptorAnimations)
        {
            if (ra.gameObject.activeInHierarchy)
            {
                currentAnimation = ra;
                return;
            }
        }
        currentAnimation = null;
    }

    public void PlayCurrentAnimation()
    {
        if (currentAnimation != null)
            currentAnimation.PlayRaptorAnimation();
        playButton.gameObject.SetActive(false);

    }

    public void ResetAnimations()
    {
        foreach (RaptorAnimation ra in raptorAnimations)
        {
            foreach (Animator a in ra.animators)
            {
                Debug.Log(a.gameObject.name);
                a.SetBool("Animate", false);
            }
        }
    }

    public void OnAutomaticHitTest(HitTestResult hit)
    {
        locatorButton.interactable = true;
    }

    public void OnContentPlaced(GameObject gO)
    {
        playButton.gameObject.SetActive(true);
        Activate(onScanObjects, false);
        Activate(onCameraObjects);
        PlayCurrentAnimation();
    }

    void Activate(GameObject[] gArray, bool active = true)
    {
        foreach (GameObject g in gArray)
        {
            g.SetActive(active);
        }
    }

    public void GoToCamera()
    {
        Activate(onCameraObjects);

    }

    public ContentPositioningBehaviour contentPlaceBehaviour;
    public PlaneFinderBehaviour planeFinder;
    public void ContentPlace()
    {
        Debug.Log("ContentPlace");
        placeHolderLata.SetActive(false);

        Vector2 vec = new Vector2(Screen.width / 2, Screen.height / 2 - Screen.height / 4);
        planeFinder.PerformHitTest(vec);
    }
}
