﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaptorTrackerHandler : MonoBehaviour
{
    public RaptorAnimation raptorAnimation;

    public void Play()
    {
        raptorAnimation.PlayRaptorAnimation();
    }
}
