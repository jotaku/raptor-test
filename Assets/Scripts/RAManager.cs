﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RAManager : MonoBehaviour
{
    public GameObject preCamera;
    public GameObject cameraUI;
    public GameObject finished;


    public void GoToPreCamera()
    {
        preCamera.SetActive(true);
        cameraUI.SetActive(false);
        finished.SetActive(false);
    }

    public void GoToCamera()
    {
        preCamera.SetActive(false);
        cameraUI.SetActive(true);
        finished.SetActive(false);
    }

    public void GoToFinished()
    {
        preCamera.SetActive(false);
        cameraUI.SetActive(false);
        finished.SetActive(true);
    }

}
