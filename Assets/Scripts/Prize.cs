﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class PrizesList
{
    public List<Prize> data;
}

[Serializable]
public class Prize 
{

    public int idpryze;

    public string name;

    public string description;


}
