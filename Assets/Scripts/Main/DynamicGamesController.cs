﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class DynamicGamesController : MonoBehaviour
{

    public Button dinoGameButton;
    public Button motoGameButton;

    public GameObject dinoGameUnlocked;
    public GameObject motoGameUnlocked;

    private void Awake()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    private void OnEnable()
    {
        CheckGames();
    }

    public void GoToDinoGame()
    {
        SceneManager.LoadSceneAsync(GamesScenes.DinoGame);
    }

    public void GoToMotoGame()
    {
        SceneManager.LoadSceneAsync(GamesScenes.MotoGame);
    }

    private void CheckGames()
    {
        if (PlayerPrefs.GetInt(DynamicContentPrefs.DinoGame, 0) == 1)
        {
            dinoGameUnlocked.SetActive(false);
            dinoGameButton.interactable = true;
        }
        else
        {
            dinoGameUnlocked.SetActive(true);
            dinoGameButton.interactable = false;
        }

        if (PlayerPrefs.GetInt(DynamicContentPrefs.MotoGame, 0) == 1)
        {
            motoGameUnlocked.SetActive(false);
            motoGameButton.interactable = true;
        }
        else
        {
            motoGameUnlocked.SetActive(true);
            motoGameButton.interactable = false;
        }
    }

    public void RecoverGames(TMP_InputField inputField)
    {
        StartCoroutine(RecoverGamesRoutine(inputField.text));
    }

    IEnumerator RecoverGamesRoutine(string email)
    {
        WWWForm form = new WWWForm();
        form.AddField("email", email);

        UnityWebRequest www = UnityWebRequest.Post("https://raptorserver-s7jlmydnpa-uc.a.run.app/users/recovergame", form);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            StartCoroutine(RecoverGamesRoutine(email));

        }
        else
        {
            Debug.Log(www.downloadHandler.text);

            if (www.responseCode == 500)
            {
                Debug.Log("ERROR EN EL GET PRICE");
            }
            else
            {
                ResponsePrizesData res = JsonUtility.FromJson<ResponsePrizesData>(www.downloadHandler.text);
                foreach (ResponsePrize r in res.data)
                {
                    Debug.Log(r.name);
                    Response(r);
                }
                CheckGames();
            }
        }
    }

    internal void Response(ResponsePrize res)
    {
        if (!string.IsNullOrEmpty(res.name))
        {
            if (res.name.Equals("Juego"))
            {
                if (res.description.Contains("Run"))
                {
                    Debug.Log("RUN");
                    PlayerPrefs.SetInt(DynamicContentPrefs.DinoGame, 1);

                }
                else
                {
                    Debug.Log("MOTO");

                    PlayerPrefs.SetInt(DynamicContentPrefs.MotoGame, 1);
                }
                PlayerPrefs.Save();
            }
        }




    }



}

public class DynamicContentPrefs
{
    public const string DinoGame = "Game_Dino_Unlocked";
    public const string MotoGame = "Game_Moto_Unlocked";
}

public class GamesScenes
{
    public const string DinoGame = "Game_Dino";
    public const string MotoGame = "Game_Moto";
}
