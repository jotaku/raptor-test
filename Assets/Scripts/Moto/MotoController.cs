﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotoController : MonoBehaviour
{
    public float forwardSpeed;
    public float laneDistance = 3f;
    public ParticleSystem explosionParticle;
    public GameObject motoRenderer;
    public AudioSource crashSound;
    public AudioSource motorSound;

    private Vector3 direction;

    private int desiredLine = 1; //0: center ; 2: right ; 1: left
    

    private MotoGameManager gameManager;
    private bool motorStart = false;

    private void Awake()
    {
        gameManager = FindObjectOfType<MotoGameManager>();
    }

    private void Update()
    {
        if (gameManager.GameRunning())
        { 
            transform.position += Vector3.forward * forwardSpeed * Time.deltaTime;

            if (!motorStart)
            {
                motorStart = true;
                motorSound.Play();
            }

            Swipe();
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision with " + collision.gameObject.name);
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("Crash");
            Crash();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("trigger with " + other.name);
        if (other.tag == "Enemy")
        {
            Debug.Log("Crash");
            Crash();
        }
    }

    private void Crash()
    {
        if (motorStart)
            motorSound.Stop();

        motoRenderer.SetActive(false);
        explosionParticle.gameObject.SetActive(true);
        motorStart = false;
        crashSound.Play();
        gameManager.ChangeState(GameState.GAMEOVER);
    }

    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;

    public void Swipe()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                //save began touch 2d point
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }
            if (t.phase == TouchPhase.Ended)
            {
                //save ended touch 2d point
                secondPressPos = new Vector2(t.position.x, t.position.y);

                //create vector from the two points
                currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                //normalize the 2d vector
                currentSwipe.Normalize();

                //swipe upwards
                if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
                {
                    Debug.Log("up swipe");
                }
                //swipe down
                if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
                {
                    Debug.Log("down swipe");
                }
                //swipe left
                if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
                {
                    Debug.Log("left swipe");
                    desiredLine--;
                    if (desiredLine == -1)
                    {
                        desiredLine = 0;
                    }
                }
                //swipe right
                if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
                {
                    Debug.Log("right swipe");
                    desiredLine++;
                    if (desiredLine == 3)
                    {
                        desiredLine = 2;
                    }
                }

                Vector3 targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;

                if (desiredLine == 0)
                {
                    targetPosition += Vector3.left * laneDistance;
                }
                else if (desiredLine == 2)
                {
                    targetPosition += Vector3.right * laneDistance;
                }

                if (transform.position == targetPosition)
                    return;

                transform.position = Vector3.Lerp(transform.position, targetPosition, 1000 * Time.deltaTime);

            }
        }
    }
}
