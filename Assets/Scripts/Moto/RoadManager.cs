﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RoadManager : MonoBehaviour
{
    [Header("Player")]
    public Transform player;

    [Header("Road")]
    public GameObject[] roadTiles;
    public int initalTilesCount = 5;
    public float tileLength = 30f;

    private float zSpawn = 0f;
    private List<GameObject> roadPool = new List<GameObject>();

    private void Awake()
    {
        for(int i = 0; i < initalTilesCount; i++)
        {
            if (i == 0)
                SpawnRoad(0);
            else
                SpawnRoad(Random.Range(1,roadTiles.Length));
        }
    }

    private void Update()
    {
        if(player.position.z > zSpawn - (initalTilesCount * tileLength))
        {
            PoolRoad();
        }
    }

    private void SpawnRoad(int index)
    {
        GameObject go = Instantiate(roadTiles[index], transform.forward * zSpawn, roadTiles[index].transform.rotation);
        zSpawn += tileLength;
        roadPool.Add(go);
    }

    private void PoolRoad()
    {
        foreach(GameObject road in roadPool)
        {
            if (IsBehind(road.transform))
            {
                road.transform.position = transform.forward * zSpawn;
                zSpawn += tileLength;
            }
        }    
    }

    private bool IsBehind(Transform target) // road behind player
    {
        Vector3 toTarget = (target.position - player.position + (Vector3.forward * tileLength)).normalized;

        if (Vector3.Dot(toTarget, player.forward) > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

}
