﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MotoGameManager : MonoBehaviour
{
    [Header("UI")]
    public GameObject startText;
    public GameObject gameoverPanel;
    public TextMeshProUGUI points;
    
    public GameState gameState = GameState.PRESTART;

    private float timer = 0f;
    private int actualPoints = 0;

    private void Awake()
    {
        Screen.orientation = ScreenOrientation.LandscapeRight;
    }

    private void Update()
    {
        if (gameState == GameState.RUNNING)
        { 
            timer += Time.deltaTime;
            if (timer >= 1)
            {
                actualPoints++;
                timer = 0;
                UpdatePoints();
            }
        }

        if ((Input.GetKeyDown(KeyCode.Space) || Input.touchCount == 1) && gameState == GameState.PRESTART)
        {
            gameState = GameState.RUNNING;
        }

        CheckState();

        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene("Main");
    }

    public bool GameRunning()
    {
        return gameState == GameState.RUNNING;
    }

    public void ChangeState(GameState newState)
    {
        gameState = newState;
    }

    public void CheckState()
    {
        switch (gameState)
        {
            case GameState.PRESTART:
                startText.SetActive(true);
                gameoverPanel.SetActive(false);
                break;
            case GameState.RUNNING:
                startText.SetActive(false);
                gameoverPanel.SetActive(false);
                break;
            case GameState.GAMEOVER:
                startText.SetActive(false);
                gameoverPanel.SetActive(true);
                break;
            default:
                break;
        }
    }

    private void UpdatePoints()
    {
        points.text = actualPoints.ToString("000000000");
    }

    public void BackButton()
    {
        SceneManager.LoadScene("Main");
    }

    public void RestartButton()
    {
        SceneManager.LoadScene(GamesScenes.MotoGame);
    }

}
