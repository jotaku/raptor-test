﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Networking;

public class Ruleta : MonoBehaviour
{

    public float spinForce = 200;

    public float spin = 5.0f;

    private bool state = false;

    public float drag = 0.9f; //rate of "slow-down"

    private float currentSpin = 0.0f; //spin-speed

    private bool spinning = false;

    public GameObject startButton;

    public GameObject stopButton;

    public GameObject selector;

    SelectPrize prize;

    int[] win = { 1, 3, 5, 7, 9 };
    int[] lose = { 0, 2, 4, 6, 8 };

    float currentResult;
    public int spinCount;
    public int maxSpinCount = 5;
    bool winPrize;

    public List<TextMeshProUGUI> prizeTexts;
    GameManager gameManager;

    public GameObject pryzeSign;

    public GameObject downloadButton;

    int idUser;
    string token;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        prize = selector.GetComponent<SelectPrize>();
        spinCount = 0;
        winPrize = false;

    }

    IEnumerator StopSpinningRoutine()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(2, 5));

        state = true;
        //	yield return new WaitForSeconds (1);

        //stopButton.SetActive (false);
        //startButton.SetActive (true);
    }

    int rouletteResult;
    int r;

    public void StartSpinning()
    {
        gameManager.SpinRoulete(idUser, token);

        state = false;
        spinning = true;
        spin = spinForce;
        currentSpin = spin;
        startButton.SetActive(false);
        //stopButton.SetActive (true);

        if (spinCount < 3)
        {
            r = UnityEngine.Random.Range(0, 4);
            rouletteResult = lose[r];
            spinCount++;
        }
        else
        {
            if ((spinCount == 4) && (winPrize == false))
            {
                r = UnityEngine.Random.Range(0, 4);
                rouletteResult = win[r];
                spinCount++;
                winPrize = true;
            }
            else
            {
                currentResult = UnityEngine.Random.value;
                if ((currentResult < 0.5f) && (!winPrize))
                {
                    r = UnityEngine.Random.Range(0, 4);
                    rouletteResult = win[r];
                    spinCount++;
                    winPrize = true;
                }
                else
                {
                    r = UnityEngine.Random.Range(0, 4);
                    rouletteResult = lose[r];
                    spinCount++;
                }
            }
        }
    }

    public void StopSpinning()
    {

        StartCoroutine(StopSpinningRoutine());
    }

    bool ExistValue(int[] a, int i)
    {
        foreach (int j in a)
        {
            if (j == i)
                return true;
        }
        return false;
    }


    void Update()
    {
        if (spinning)
        {

            if ((state == true))//&& (prize.prize == rouletteResult)) 
            {
                currentSpin = currentSpin * drag;
            }

            if (currentSpin > 0.01)
            {
                transform.Rotate(0, 0, Time.deltaTime * currentSpin);
            }
            else
            {
                if (ExistValue(lose, rouletteResult))
                {
                    //ruletaManager.GetComponent<RuletaManager> ().Play (spinCount >= maxSpinCount);
                    if (spinCount < maxSpinCount)
                    {

                        rouletteResult = 99;
                    }
                    else
                    {
                        Debug.Log("ENTRO ACA");
                        //ruletaManager.GetComponent<RuletaManager> ().ShowPoints ();
                        spinCount = 0;
                        rouletteResult = 99;
                        winPrize = false;
                    }
                }
                else if (spinCount >= maxSpinCount)
                {
                    //ruletaManager.GetComponent<RuletaManager> ().Play (spinCount >= maxSpinCount);
                    //ruletaManager.GetComponent<RuletaManager> ().ShowPoints ();
                    spinCount = 0;
                    rouletteResult = 99;
                    winPrize = false;
                }
            }
        }

    }

    public TextMeshProUGUI prizeTitle;
    public TextMeshProUGUI prizeDescription;
    public Image prizeImage;

    public GameObject errorSign;
    internal void Response(ResponsePrize res)
    {
        if (res.win == 1)
        {
            StopSpinning();
            if (!string.IsNullOrEmpty(res.name))
            {
                prizeTitle.text = res.name;
                prizeDescription.text = res.description;
                pryzeSign.SetActive(true);

                if (!string.IsNullOrEmpty(res.link))
                {
                    downloadButton.GetComponent<ButtonLink>().url = res.link;
                    downloadButton.SetActive(true);
                }
                if (!string.IsNullOrEmpty(res.image))
                {
                    StartCoroutine(LoadImage(res.image, prizeImage));
                }
                if (res.name.Equals("Juego"))
                {
                    if (res.description.Contains("Run"))
                    {
                        PlayerPrefs.SetInt(DynamicContentPrefs.DinoGame, 1);

                    }
                    else
                    {
                        PlayerPrefs.SetInt(DynamicContentPrefs.MotoGame, 1);
                    }
                    PlayerPrefs.Save();
                }
            }
            else
            {
                errorSign.SetActive(true);
            }

        }
        else
        {
            Debug.Log("DIDNT WIN");
        }
    }

    public void SetPrizes(List<Prize> prizes)
    {
        int i = 0;
        foreach (TextMeshProUGUI text in prizeTexts)
        {
            text.SetText(prizes[i].name);
            i++;
            if (i == prizes.Count)
                i = 0;

        }
    }

    public void Participate(int id, string t)
    {
        idUser = id;
        token = t;
    }


    IEnumerator LoadImage(string imgUrl, Image img)
    {
        UnityWebRequest www = UnityWebRequest.Get(imgUrl);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            var tex = new Texture2D(200, 200);
            tex.LoadImage(www.downloadHandler.data);
            img.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
        }
    }





}