﻿using System;
using System.Collections.Generic;

[Serializable]
public class Response
{
    public int iduser;
    public string data;
    public string token;
    public string expirensIn;


}

[Serializable]
public class ResponsePrizesData
{
    public List<ResponsePrize> data;


}


[Serializable]
public class ResponsePrizeData
{
    public ResponsePrize data;


}

[Serializable]
public class ResponsePrize
{
    public int idplay;
    public string date;
    public int win;
    public string name;
    public string image;
    public string description;
    public string link;


}