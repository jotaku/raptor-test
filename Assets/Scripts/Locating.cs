﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Net.Http;

public class Locating : MonoBehaviour
{

    GameManager gameManager;

    void Start()
    {
        gameManager = GetComponent<GameManager>();
        DetectCountryAsync();
    }

    async void DetectCountryAsync()
    {
        var httpClient = new HttpClient();
        var country = await httpClient.GetStringAsync("https://api.ipdata.co/country_code?api-key=test");
        Debug.Log($"My country is: {country}");
        gameManager.SetCountry(country);

        /*
                UnityWebRequest request = UnityWebRequest.Get("https://api.ipdata.co/country_name?api-key=a6d8b6393d7efa543e322ca0bc5aec52cb4dcf62e9b7538df6833e1d");

                yield return request.SendWebRequest();
                Debug.Log("Locating...");

                if (request.isNetworkError || request.isHttpError)
                {
                    Debug.Log(request.error);
                }
                else
                {
                    if (request.isDone)
                    {
                        Country res = JsonUtility.FromJson<Country>(request.downloadHandler.text);
                        Debug.Log(res.countryCode);
                        Debug.Log(res.country);
                    }
                }*/
    }
}
