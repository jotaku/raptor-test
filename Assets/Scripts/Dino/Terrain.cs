﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Terrain : MonoBehaviour
{
    public List<GameObject> enemySpawn;

    public void DeactivateEnemies()
    {
        foreach (var enemy in enemySpawn)
        {
            enemy.SetActive(false);
        }
    }
}
