﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class DinoGameManager : MonoBehaviour
{
    [Header("UI")]
    public TextMeshProUGUI startText;
    public TextMeshProUGUI pointsDisplay;

    public GameObject gameoverPanel;

    [Header("Timer settings")]
    public float duration = 1f;

    [Header("Difficulty")]
    [SerializeField] private int increaseSpeedPoints;
    [SerializeField] private float speedIncrease;

    public bool GameStart { get; private set; }
    public bool GameOver { get; set; }

    private float timer;
    private int actualPoints = 0;
    private Dino dinoPlayer;
    private bool increasingSpeed = false;

    private void Awake()
    {
        Screen.orientation =  ScreenOrientation.LandscapeRight;
        GameStart = false;
        dinoPlayer = FindObjectOfType<Dino>();
    }

    private void Update()
    {
        if (!GameStart && Input.GetKeyDown(KeyCode.Space) || Input.touchCount == 1)
        {
            GameStart = true;
            startText.gameObject.SetActive(false);
        }

        if (GameStart && !GameOver)
        {
            timer += Time.deltaTime;
            if (timer >= duration)
            {
                actualPoints++;
                UpdatePoints();
                timer = 0f;
            }
            IncreaseSpeed();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene("Main");

    }

    public void EnableGameoverPanel()
    {
        gameoverPanel.SetActive(true);
    }

    public void BackButton()
    {
        SceneManager.LoadScene("Main");
    }

    public void RestartButton()
    {
        SceneManager.LoadScene(GamesScenes.DinoGame);
    }

    private void UpdatePoints()
    {
        pointsDisplay.text = actualPoints.ToString("0000");
    }

    private void IncreaseSpeed()
    {
        //if (actualPoints > 0 && actualPoints % increaseSpeedPoints == 0)
        //{

        //}
    }

}
