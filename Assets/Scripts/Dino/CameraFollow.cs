﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Vector3 direction = Vector3.zero;
    public GameObject player;        
    private Vector3 offset;
    //private float smoothSpeed = 10f;

    void Start()
    {        
        offset = transform.position - player.transform.position;
    }

    void LateUpdate()
    {
        // Vector3 desiredPosition = player.transform.position + offset;
        //Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);
        //transform.position = smoothedPosition;
        Vector3 desiredPosition = new Vector3((player.transform.position.x + offset.x) * direction.x, transform.position.y, (player.transform.position.z + offset.z) * direction.z);
        transform.position = desiredPosition;
    }
}
