﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

public class TerrainPool : MonoBehaviour
{
    public int terrainSize = 5;
    public GameObject terrainPrefab;
    public Transform parent;
    public float hiddenDistance = 15f;
    public GameObject[] enemies;
    [Range(0, 100)]
    public int minPositionToSpawn;
    [Range(0, 100)]
    public int maxPositionToSpawn;
    public Dino dino;

    private GameObject[] terrains;
    private Vector2 objectPoolPosition = new Vector2(-50, 0);
    private float distanceOfTerrains = 100f;
    private Vector2 lastPositionedObj;
    private int lastPositionEnemySpawned;

    private float oneEnemmyProbability = 0.5f;
    private float twoEnemmyProbability = 0.7f;
    private float threeEnemmyProbability = 0.8f;

    private void Awake()
    {
        InitPool();
    }

    private void Update()
    {
        MovePoolObj();
    }

    private void InitPool()
    {
        terrains = new GameObject[terrainSize];
        for (int i = 0; i < terrainSize; i++)
        {
            lastPositionedObj = objectPoolPosition + (Vector2.right *(distanceOfTerrains* i));
            terrains[i] = (GameObject)Instantiate(terrainPrefab, lastPositionedObj, Quaternion.identity, parent);
            SpawnEnemy(i);
        }
    }

    private void MovePoolObj()
    {
        for (int i = 0 ; i < terrains.Length; i++)
        {
            if (terrains[i].transform.localPosition.x < dino.transform.position.x - hiddenDistance)
            {
                terrains[i].GetComponent<Terrain>().DeactivateEnemies();
                terrains[i].SetActive(false);
                lastPositionedObj += Vector2.right * distanceOfTerrains; 
                terrains[i].transform.localPosition = lastPositionedObj;
                SpawnEnemy(i);
                terrains[i].SetActive(true);
            }
        }
    }

    private void SpawnEnemy(int currentTerrain)
    {
        if (currentTerrain != 0)
        {
            float random = Random.Range(0f, 1f);
            int distanceToSpawn = maxPositionToSpawn - minPositionToSpawn;
            Terrain terrain = terrains[currentTerrain].GetComponent<Terrain>();

            if (random <= oneEnemmyProbability)
            {
                Vector3 positionToSpawn = new Vector3(distanceToSpawn / 2, 0, dino.transform.position.z);
                if (terrain.enemySpawn.Count == 0)
                {           
                    GameObject go = Instantiate(enemies[Random.Range(0, enemies.Length)], terrain.transform.TransformPoint(positionToSpawn), Quaternion.identity, terrains[currentTerrain].transform);
                    terrain.enemySpawn.Add(go); 
                }
                else
                {
                    DisableEnemies(currentTerrain);

                    GameObject g = terrain.enemySpawn[Random.Range(0, terrain.enemySpawn.Count)];
                    g.transform.position = terrain.transform.TransformPoint(positionToSpawn);
                    g.SetActive(true);
                }
                
            }
            else if (random <= twoEnemmyProbability)
            {
                Vector3 positionToSpawn1 = new Vector3(minPositionToSpawn, 0, dino.transform.position.z);
                Vector3 positionToSpawn2 = new Vector3(maxPositionToSpawn, 0, dino.transform.position.z);

                if (terrain.enemySpawn.Count == 0)
                {
                    GameObject g1 = Instantiate(enemies[Random.Range(0, enemies.Length)], terrain.transform.TransformPoint(positionToSpawn1), Quaternion.identity, terrains[currentTerrain].transform);
                    terrain.enemySpawn.Add(g1);

                    GameObject g2 = Instantiate(enemies[Random.Range(0, enemies.Length)], terrain.transform.TransformPoint(positionToSpawn2), Quaternion.identity, terrains[currentTerrain].transform);
                    terrain.enemySpawn.Add(g2);
                }
                else if (terrain.enemySpawn.Count == 1)
                {
                    terrain.enemySpawn[0].transform.position = terrain.transform.TransformPoint(positionToSpawn1);

                    GameObject g = Instantiate(enemies[Random.Range(0, enemies.Length)], terrain.transform.TransformPoint(positionToSpawn2), Quaternion.identity, terrains[currentTerrain].transform);
                    terrain.enemySpawn.Add(g);
                }
                else if (terrain.enemySpawn.Count == 2)
                {
                    terrain.enemySpawn[0].transform.position = terrain.transform.TransformPoint(positionToSpawn1);
                    terrain.enemySpawn[1].transform.position = terrain.transform.TransformPoint(positionToSpawn2);
                }
                else
                {
                    DisableEnemies(currentTerrain);

                    terrain.enemySpawn[Random.Range(0,2)].transform.position = terrain.transform.TransformPoint(positionToSpawn1);
                    terrain.enemySpawn[2].transform.position = terrain.transform.TransformPoint(positionToSpawn2);
                }
               
            }
            else
            {
                Vector3 positionToSpawn1 = new Vector3(minPositionToSpawn, 0, dino.transform.position.z);
                Vector3 positionToSpawn2 = new Vector3(maxPositionToSpawn, 0, dino.transform.position.z);
                Vector3 positionToSpawn3 = new Vector3(((maxPositionToSpawn-minPositionToSpawn)/2), 0, dino.transform.position.z);

                if (terrain.enemySpawn.Count == 0)
                {
                    GameObject g1 = Instantiate(enemies[Random.Range(0, enemies.Length)], terrain.transform.TransformPoint(positionToSpawn1), Quaternion.identity, terrains[currentTerrain].transform);
                    terrain.enemySpawn.Add(g1);

                    GameObject g2 = Instantiate(enemies[Random.Range(0, enemies.Length)], terrain.transform.TransformPoint(positionToSpawn2), Quaternion.identity, terrains[currentTerrain].transform);
                    terrain.enemySpawn.Add(g2);
                    
                    GameObject g3 = Instantiate(enemies[Random.Range(0, enemies.Length)], terrain.transform.TransformPoint(positionToSpawn3), Quaternion.identity, terrains[currentTerrain].transform);
                    terrain.enemySpawn.Add(g3);
                }
                else if (terrain.enemySpawn.Count == 1)
                {
                    terrain.enemySpawn[0].transform.position = terrain.transform.TransformPoint(positionToSpawn1);

                    GameObject g2 = Instantiate(enemies[Random.Range(0, enemies.Length)], terrain.transform.TransformPoint(positionToSpawn2), Quaternion.identity, terrains[currentTerrain].transform);
                    terrain.enemySpawn.Add(g2);

                    GameObject g3 = Instantiate(enemies[Random.Range(0, enemies.Length)], terrain.transform.TransformPoint(positionToSpawn3), Quaternion.identity, terrains[currentTerrain].transform);
                    terrain.enemySpawn.Add(g3);

                }
                else if (terrain.enemySpawn.Count == 2)
                {
                    terrain.enemySpawn[0].transform.position = terrain.transform.TransformPoint(positionToSpawn1);
                    terrain.enemySpawn[1].transform.position = terrain.transform.TransformPoint(positionToSpawn2);

                    GameObject g3 = Instantiate(enemies[Random.Range(0, enemies.Length)], terrain.transform.TransformPoint(positionToSpawn3), Quaternion.identity, terrains[currentTerrain].transform);
                    terrain.enemySpawn.Add(g3);
                }
                else
                {

                    terrain.enemySpawn[0].transform.position = terrain.transform.TransformPoint(positionToSpawn1);
                    terrain.enemySpawn[1].transform.position = terrain.transform.TransformPoint(positionToSpawn2);
                    terrain.enemySpawn[2].transform.position = terrain.transform.TransformPoint(positionToSpawn3);
                }
            }
        }
    }

    private void DisableEnemies(int currentTerrain)
    {
        Terrain terrain = terrains[currentTerrain].GetComponent<Terrain>();
        foreach (GameObject gs in terrain.enemySpawn)
        {
            gs.SetActive(false);
        }
    }


}