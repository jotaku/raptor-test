﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;

[RequireComponent(typeof(Rigidbody))]
public class Dino : MonoBehaviour
{
    public DinoStats stats;
    public int jumpsPerIncrease = 3;

    private Rigidbody rb;
    private Animator anim;
    private bool jump;
    private float distToGround;
    private DinoGameManager gameManager;
    private bool isGrounded = false;
    private bool isDead = false;
    private bool jumpKeyHeld = false;
    private Vector2 counterJumpForce = new Vector2(0, -1);
    private int correctJumps = 0;
    

    private void Awake()
    {
        anim = GetComponent<Animator>();
        distToGround = GetComponent<Collider>().bounds.extents.y;
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotation;

        gameManager = FindObjectOfType<DinoGameManager>();
    }

    private void Update()
    {
        if (gameManager.GameStart && !isDead)
        {
            if ((Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) && isGrounded)
            {
                anim.SetTrigger(DinoAnimationParameters.Jumping);
  
                if (isGrounded)
                {
                    jump = true;
                }
            }

            if (rb.velocity.x > 0 && !jump && isGrounded)
            {
                anim.SetBool(DinoAnimationParameters.Running, true);
            }
            else
            {
                anim.SetBool(DinoAnimationParameters.Running, false);
            }

            if (correctJumps == jumpsPerIncrease)
            {
                stats.speed++;
                jumpsPerIncrease += jumpsPerIncrease;
            }
        }
    }
    private void FixedUpdate()
    {
        if (gameManager.GameStart && !isDead)
        {
            if (jump)
            {
                rb.velocity += stats.jumpForce * Vector3.up;
                jump = false;
            }

            rb.MovePosition(transform.position + transform.forward * -1 * stats.speed * Time.fixedDeltaTime);        
        }
    }

    public void IncreaseCorrectJumps()
    {
        correctJumps++;
    }

    private void Die()
    {
        if (!isDead)
        { 
            anim.SetBool(DinoAnimationParameters.Running, false);
            anim.SetBool(DinoAnimationParameters.Die, true);
            isDead = true;
            gameManager.EnableGameoverPanel();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == Layers.Floor)
        {
            isGrounded = true;
        }

        if (collision.gameObject.tag == Tags.Enemy)
        {
            Debug.Log("Game over");
            Die();
            gameManager.GameOver = true;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.layer == Layers.Floor)
        {
            isGrounded = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == Layers.Floor)
        {
            isGrounded = false;
        }
    }
}

[System.Serializable]
public class DinoStats
{
    public float speed;
    public float jumpForce;
}

public class DinoAnimationParameters
{
    public const string Running = "Running";
    public const string Jumping = "Jumping";
    public const string Die = "Die";
}

public class Tags
{
    public const string Enemy = "Enemy";
}

public class Layers
{
    public const int Floor = 8;
}
