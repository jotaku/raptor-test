﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class Form : MonoBehaviour
{
    public TMP_InputField nameField;
    public TMP_InputField emailField;
    public TMP_InputField dpiField;
    public TMP_InputField phoneField;

    public TMP_Text errorText;

    public Toggle toggle;

    GameManager gameManager;

    public GameObject loading;


    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SignIn()
    {
        if (Validate())
        {
            gameManager.Participate(nameField.text, emailField.text, dpiField.text, phoneField.text);
        }
    }

    public bool Validate()
    {
        if (string.IsNullOrEmpty(nameField.text) ||
           string.IsNullOrEmpty(emailField.text) ||
           string.IsNullOrEmpty(dpiField.text) ||
           string.IsNullOrEmpty(phoneField.text))
        {
            errorText.text = "* Complete todos los campos";
            return false;
        }

        if (!toggle.isOn)
        {
            errorText.text = "* Para participar acepte los terminos y condiciones";
            return false;
        }

        if (!IsValidEmail(emailField.text))
        {
            errorText.text = "* Email inválido";
            return false;
        }

        if (gameManager.idRegion == "GT")
        {
            if (dpiField.text.Length != 13)
            {
                errorText.text = "* DPI inválido";
                return false;
            }
        }

        return true;
    }



    public static bool IsValidEmail(string email)
    {
        if (string.IsNullOrWhiteSpace(email))
            return false;

        try
        {
            // Normalize the domain
            email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                  RegexOptions.None, TimeSpan.FromMilliseconds(200));

            // Examines the domain part of the email and normalizes it.
            string DomainMapper(Match match)
            {
                // Use IdnMapping class to convert Unicode domain names.
                var idn = new IdnMapping();

                // Pull out and process domain name (throws ArgumentException on invalid)
                var domainName = idn.GetAscii(match.Groups[2].Value);

                return match.Groups[1].Value + domainName;
            }
        }
        catch (RegexMatchTimeoutException e)
        {
            return false;
        }
        catch (ArgumentException e)
        {
            return false;
        }

        try
        {
            return Regex.IsMatch(email,
                @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
        catch (RegexMatchTimeoutException)
        {
            return false;
        }
    }

}
